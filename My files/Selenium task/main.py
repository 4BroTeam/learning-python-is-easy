from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as condition
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select

class InitDriver:
    """
    Class initiating WebDriver with Edge browser
    """    
    link = "http://automationpractice.com/index.php"
    driver = webdriver.Edge("edgedriver path location")
    wait = WebDriverWait(driver, 5)

    def timeout(self, time):
        """
        Method to handle implicit waits

        Args:
            time ([int]): seconds to wait
        """        
        self.driver.implicitly_wait(time)
            
