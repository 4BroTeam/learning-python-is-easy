from itertools import dropwhile
from re import search

class Message:
    
    """
    Class which holds data extracted from .txt file

    The extracted data is used in contact form in the web page.
    Initializing class instance opens the .txt file and finds
    lines with proper prefixes. Those prefixes holds the source
    text for the proper filling contact form in web store.

    First line is for email address (mandatory)
    Second line is for order reference number (not mandatory)
    Third line for path to file that is going to be attached (not mandatory)
    Fourth line is for subject choose 'CS' for Customer service, 'WM' for Webmaster
    Last line is for message content (cannot be empty) only in one line
    All lines starting with '#' are ignored same with empty lines
    If any part of form is going to be empty, the proper line should have 'NULL'
    Each line starts with tag name: 'email:' for e-mail, 'order:' for order number etc
    Example below

    email:some@email.address
    order:988296365
    file:NULL
    subject:CS
    text:This is the message line, it should be in same line,
        disregarding the amount of characters

    """
    def __init__(self, source_file: str) -> None:
        """
        Initialization of class

        Passing path to a file as string, 

        Args:
            source_file (str): string with file path to extract
        """        
        with open(source_file) as file:
            extracted_data = []
            key_words_list = ("email:", "order:", 
                            "file:", "subject:", "text:")
            for line in dropwhile(lambda x: x.startswith(("#", "\n")),file):
                extracted_data.append(Message.check_line(line, key_words_list))
            self.email = extracted_data[0]
            self.order_num = extracted_data[1]
            self.attach_file_path = extracted_data[2]
            self.subjects = extracted_data[3]
            self.messsage_text = extracted_data[4]
            del extracted_data

    @staticmethod
    def check_line(line, key_list):
        """
        Method removing prefixes from file lines

        Method iterates keyword stored in key_list argument
        if match in examined line removing only first occurrence.
        If line has "NULL" as value, function return None

        Args:
            line ([str]): passed read line from file
            key_list ([Iterable]): preferable tuple with keyword strings

        Returns:
            [str]: String or None
        """        
        for key_word in key_list:
            if line.startswith(key_word):
                extracted_data = line.replace(key_word,"", 1).strip("\n")
                if extracted_data.lower() == "null":
                    return None
                else:
                    return extracted_data

    @staticmethod
    def email_check(email):       
        """
        Function checks if e-mail is valid

        Function checks if the provided email has the required
        form returning True, raise AssertError otherwise.

        Args:
            email ([str]): e-mail to check
            
        Raises:
            AssertError: If invalid email

        Returns:
            [bool]: Returns True if valid email
        """    
        chars = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"
        if search(chars, email) is None:
            assert False , "Invalid email"
        else:
            return True
