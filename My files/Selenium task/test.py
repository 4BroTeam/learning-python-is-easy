import unittest
import main as S
from message import Message
from datetime import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as condition

class PageTests(unittest.TestCase):

    data = Message("message.txt")

    def setUp(self) -> None:
        self.page = S.InitDriver()
        self.driver = self.page.driver
        self.driver.get("http://automationpractice.com/index.php")

    def test_navigate(self):
        """
        Testing navigation to the contact form
        """
        contact_link = self.driver.find_element_by_id("contact-link")
        contact_link.click()
        self.page.timeout(self.driver, 5)
        customer_service_title = self.driver.find_element_by_tag_name("h1")
        assert customer_service_title.text == "CUSTOMER SERVICE - CONTACT US", \
            "Contact form not reached"

    @unittest.expectedFailure
    def test_submit_empty(self):
        """
        Test submitting empty contact form

        Testing navigating to contact form and submit empty form
        If alert shows invalid email test is failed
        """
        contact_link = self.driver.find_element_by_id("contact-link")
        contact_link.click()
        self.page.timeout(self.driver, 5)
        self.page.wait.until(condition.presence_of_element_located(
            (By.ID, "submitMessage")
            ))
        submit_button = self.driver.find_element_by_id("submitMessage")
        submit_button.click()
        self.page.timeout(self.driver, 5)
        alert = self.driver.find_element_by_class_name(
            "alert alert-danger")
        alert_message = alert.text
        if alert.get_attribute("class") == "alert alert-danger":
            assert False, alert_message

    @unittest.expectedFailure
    def test_submit_invalid_email(self):
        """
        Test submitting invalid email (internal check)

        Testing navigation to contact form and submiting without valid email
        Email validation from imported module
        """
        contact_link = self.page.driver.find_element_by_id("contact-link")
        contact_link.click()
        self.page.timeout(self.driver, 5)
        e_mail_field = self.page.driver.find_element_by_id("email")
        invalid_email = "invalid@email/com"
        Message.email_check(invalid_email)
        e_mail_field.send_keys(invalid_email)

    def test_submit_invalid_email_V2(self):
        """
        Test submitting invalid email (web check)

        Testing navigation to contact form and submiting without valid email
        """
        contact_link = self.page.driver.find_element_by_id("contact-link")
        contact_link.click()
        self.page.timeout(self.driver, 5)
        e_mail_field = self.page.driver.find_element_by_id("email")
        invalid_email = "invalid@email/com"
        e_mail_field.send_keys(invalid_email)
        submit_button = self.driver.find_element_by_id("submitMessage")
        submit_button.click()
        self.page.timeout(self.driver, 5)
        alert = self.driver.find_element_by_class_name(
            "alert alert-danger")
        alert_message = alert.text
        if alert.get_attribute("class") == "alert alert-danger":
            assert False, alert_message

    def test_full_form_submit(self):
        """
        Test full form filling

        Testing filling the contact form with provided data extracted
        by Message class.
        """        
        contact_link = self.page.driver.find_element_by_id("contact-link")
        contact_link.click()
        self.page.timeout(self.driver, 5)
        e_mail_field = self.page.driver.find_element_by_id("email")
        e_mail_field.send_keys(self.data.email)
        order_field = self.page.driver.find_element_by_id("id_order")
        order_field.send_keys(self.data.order_num)
        message_text_field = self.page.driver.find_element_by_id("message")
        message_text_field.send_keys(self.data.messsage_text)
        selection = S.Select(self.page.driver.find_element_by_tag_name("select"))
        if self.data.subjects == "CS":
            selection.select_by_value("2")
        elif self.data.subjects == "WM":
            selection.select_by_value("1")
        else:
            selection.select_by_value("0")
        submit_button = self.driver.find_element_by_id("submitMessage")
        submit_button.click()


    def tearDown(self):
        self.page.driver.quit()

with open("python/seleniumtask/log.txt", "a") as log:
    time = datetime.now().strftime("%Y-%m-%d %H:%M:%S").rstrip(".")
    log.writelines((f"\nTest time: {str(time)}:","\n\n"))
    logs = unittest.TextTestRunner(
        log,
        verbosity=2,
        descriptions=True
    )
    unittest.main(testRunner=logs)
