class UserData:
    """
    Class to handle user input data 

    String as the source of message

    The inputted data is stored as class attribute
    """

    def __init__(self, text):
        spaces2 = text.count("  ")
        if spaces2 > 0:
            txt = text.replace("  " * spaces2, "")
            self.data = input(txt).strip()
        else:
            self.data = input(text).strip()

    def convert_to_int(self):
        """
        Converts the given input to integer

        Returns:
            [int]
        """
        return int(self.data)
        
    def convert_to_float(self):
        return float(self.data)

    def strip_chars(self, chars=" "):
        return self.data.strip(chars)
