from enum import Enum, IntEnum
from python.Smartphone import UserData
from secrets import token_hex
from random import randint
# App to handle bank operations
# Handle dictionary to update the xml file (splitting words, iban)

class BankAccount:
    def __init__(self):
        self.balance = float()
        self.currency = "PLN"

class BankApp:
    """
    Class for handling banking operations
    """

    def __init__(self):
        """[summary]
        """
        self.receivers = {}  # holds specific data from XML file
        # data about the initiate info hold in encrypted file

    def __str__(self):
        # print((self.balance, self.currency))

    def check_recipient_hash(self):
        hash_id = token_hex(8)
        while True:
            if hash_id in self.receivers:
                hash_id = token_hex(8)
            else:
                break
        return hash_id

    def add_receiver(self, receiver_iban):
        hash_id = self.check_recipient_hash()
        if receiver_iban not in self.receivers:  # try to check dictionary
            receiver_name = UserData("Input full name (name, last name): ")
            decision = input("Do You want to add address?\nY/N: ").strip()
            if decision.lower() in ("yes", "y"):
                receiver_address = {"street": UserData("Input street name only: "),
                                    "house_no": UserData("Input house number/flat number: "),
                                    "postal_no": UserData("Type postal/zip code: "),
                                    "city": UserData("Type location name: ")}
                self.receivers.update({hash_id: {"name": receiver_name,
                                                 "iban": receiver_iban,
                                                 "address": receiver_address}
                                       })
            else:
                self.receivers.update({hash_id: {"name": receiver_name,
                                                 "iban": receiver_iban,
                                                 "address": None}
                                       })
        else:
            print("The recipient is already declared")
            decision = UserData("Do You want to update recipient?\nY/N: ")
            if decision.data.lower() in ("yes", "y"):
                self.update_receiver(hash_id)

    def update_receiver(self, hash_id):
        DataMenu = IntEnum("Data", "Name, IBAN, Address, Done")
        Menu = list(enumerate(DataMenu._member_names_,
                              start=1))  # any sense of that?
        for _ in Menu:
            print(f"{Menu[Menu.index(_)][0]}: {Menu[Menu.index(_)][1]}")
            # enumerate choice of which data to modify
        while True:
            choice = UserData(
                "Choose which data You want to update: ")
            if choice.data.lower().capitalize() == DataMenu.Name:  # update name
                new_name = UserData("Type new name: ")
                self.receivers[hash_id]["name"] = new_name.data.lower().title()
            elif choice.data.lower().capitalize() in DataMenu.IBAN:
                new_iban = UserData("Type new IBAN number: ")
                BankApp.check_iban(new_iban)
                self.receivers[hash_id]["iban"] = new_iban.data
            elif choice.data.lower().capitalize() in Menu[Address]:
                street = UserData("Input street name only: ")
                house_no = UserData("Input house number/flat number: ")
                postal_no = UserData("Type postal/zip code: ")
                city = UserData("Type location name: ")
                new_address = {"street": street.data,
                                "house_no": house_no.data,
                                "postal_no": postal_no.data,
                                "city": city.data}
                self.receivers[hash_id]["address"] = new_address
            elif choice.data.lower().capitalize() in Menu[Done]:
                break

    @staticmethod
    def transaction():
        WireType = IntEnum("Transfer", "BLIK, Mobile transfer, Regular transfer,\
        International transfer, Internal transfer")
        Menu = list(enumerate(WireType._member_names_, start=1))
        for _ in Menu:
            print(f"{Menu[Menu.index(_)][0]}: {Menu[Menu.index(_)][1]}") # enumerate choice of wire types
        choice = UserData("Choose the type of transfer")
        if in Manu[BLIK]# blik transfer
        if in Menu[Mobile]# mobile transfer
        if in Menu[Regular]# regular transfer
        if in Menu[Internationl]# international transfer
        if in Menu[Internal]# Internal Wire

    @staticmethod
    def blik_code():
        _ = [str(randint(0, 9)) for _ in range(6)]
        blik = ""
        for x in _:
            blik = blik + x
        return blik
        

    @staticmethod
    def mobile_wire(number):
        pass

    @staticmethod
    def check_iban(iban):  # checking if iban has proper amount of digits
        if iban in "":
            pass
        # checking and assigning bank to the iban if possible

    @staticmethod
    def check_receiver(**receiver_data):  # m
        pass


acc = BankApp()
iban = 45663221489
acc.add_receiver(iban)
