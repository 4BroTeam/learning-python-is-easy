# App to handle phone contacts
# concatenation instead of replace()
class ContactsApp:

    def __init__(self):
        self.contacts = {  # name: {number : (number), email: email}
        }  # handle json

    def __str__(self):
        print(f"List of Your contacts:\n{self.contacts.iterkeys()}")

    def __len__(self):
        pass

    def view_contacts(self):
        searching = UserData("Search by contact name:\n")
        for contact in self.contacts:
            if searching.data in contact:
                view = []
                view.append(contact)
            print(enumerate(view, start=1), sep="\n")

    def add_contact(self):
        contact_name = UserData("Add contact name: ")
        if self.check_contact(contact_name.lower()) == False:  # new contact
            contact_number = UserData("Add phone number \
                            without country code (preferably): ")
            # check_number() handle number with country code already added
            # num_country_prefix()x
            contact_number_code = UserData("Add country code: ")
            contact_email = UserData("Add e-mail address")
            self.contacts.update({contact_name: {
                "number": int((contact_number_code +
                               contact_number.data).strip()),
                "email": contact_email.data}
            })

    def check_contact(self, contact_name: str):
        if contact_name.capitalize() in self.contacts:
            print("Contact name exists. Please choose other name \
                or update this one".replace(" " * 12, ""))
            choice = input("Update contact: \
add new number - 'yes'\
                        replace number - 'no'\n").strip()
            if choice.lower() in ("yes", "y"):  # adding new number
                contact_number = input("Add phone number \
                without country code (preferably): ").replace(" " * (4 * 4), "")
                # function to handle country codes
                contact_number_code = input("Add country code: ")
                old_num = self.contacts[contact_name]["number"]
                self.contacts.update({contact_name: {
                    "number": (old_num, int((contact_number_code +
                                             contact_number).strip())),
                    "email": contact_email}})
            else:  # replacing old number
                self.contacts.update({contact_name: {
                    "number": int((contact_number_code +
                                   contact_number).strip()),
                    "email": contact_email}})

            return True
        else:
            return False

    @staticmethod
    def check_number(number: any):
        if len(number.lstrip("+")) == 9:
            pass

    @staticmethod
    def check_email(email: str):
        if "@" in email.strip():
            if "." in email[email.index("@"), -1].strip():
                if " " not in email[0, email.index("@")].strip():
                    return True
            else:
                return False
        else:
            return False

    @staticmethod
    def num_country_prefix():
        country_codes = ()  # tuple of tuples with pairs country code and prefix
        # add country prefix to number
        # check if prefix is already added
