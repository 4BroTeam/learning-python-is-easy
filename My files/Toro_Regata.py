import random
import enum
from typing import Iterator


class Rocket:

    def __init__(self, parameter_list: Iterator):
        """
        __init__(self -> class object | parameter_list:
        object -> iterable (list, tuple, dict))
        returns : None

        Each have it's own, kind of unique ,attributes all placed in
        list or tuple in this order or not if using dictionary:

        name - str | name of rocket
        fuel_cap - int | capacity of fuel tank
        perf - int or float | fuel consumption per 1 meter traveled
            lower = better
        velocity - int | the speed of rocket meters done per second
        start_chance - float (<1) or int (<100) | possibility of lift off,
                        higher = better
        pilot name - cosmetic information
        height = set to value 0 increase in time, distance  traveled in race
        move = get int | value from variable called "moves"
        """
        if type(parameter_list) in (list, tuple):
            self.name = parameter_list[0]
            self.fuel_cap = parameter_list[1]
            self.perf = parameter_list[2]
            self.velocity = parameter_list[3]
            self.start_chance = parameter_list[4]
            self.pilot = parameter_list[5]
        if type(parameter_list) == dict:
            self.name = parameter_list["name"]
            self.fuel_cap = parameter_list["fuel_cap"]
            self.perf = parameter_list["perf"]
            self.velocity = parameter_list["velocity"]
            self.start_chance = parameter_list["start_chance"]
            self.pilot = parameter_list["pilot"]
        self.height = 0
        self.move = moves

    def startup(self):
        """
        startup(self -> class object)
        return : bool value

        Method to define if rocket will even start
        The chance set in class can be int or float
        Each try reduces self.move amount by 1
        Returns boolean value only
        """
        text_Engines_on = ["Yes!! {} go!",
                           "{} is spooling engines!",
                           "That's the sound of {} drive starting!"]
        text_Engines_fail = ["Oh no! {} failed to start",
                             "Such a shame! {} doesn't even start...",
                             "This time {} remains still.."]
        fail = random.random()
        if type(self.start_chance) == float:
            if fail <= self.start_chance:
                self.move -= 1
                print((random.choice(text_Engines_on)).format(self.name))
                return True
            else:
                self.move -= 1
                print((random.choice(text_Engines_fail)).format(self.name))
                return False
        else:
            if fail <= (self.start_chance / 100):
                self.move -= 1
                print((random.choice(text_Engines_on)).format(self.name))
                return True
            else:
                self.move -= 1
                print((random.choice(text_Engines_fail)).format(self.name))
                return False

    def check_fuel_state(self, time):
        """
        check_fuel_state( self -> class object | time -> int )
        return : bool False value or float value

        Method to determine the remaining fuel state
        for specified time of flight.
        Returns boolean value (to determine the success of flight)
        otherwise the fuel needed for the current flight.
        """
        text_FuelMessage_full = [
            "What a fuel burn for {}!",
            "This is how it's done! {} fly away!",
            "Such a great flight for {}!",
            "Wooah! {} just flew so far away!!"
        ]
        text_FuelMessage_partial = [
            "{} has not enough fuel for full flight!\n \
                Will fly till run out of fuel!!",
            "Is this true? Sadly yes, {} will fly shorter",
            "What happened? {} flew not so far!",
            "I see this race is ending for {}."
        ]
        text_FuelMessage_shortage = [
            "{} has not enough fuel to start! \
                This is the end of race.",
            "Not enough fuel for {} to fly. Race is done.",
            "{} hasn't got enough fuel to move anymore",
            "There is too little in tank left {} to start"
        ]
        text_FuelMessage_empty = [
            "Tank is empty! {} no go!",
            "{} need to refuel but can't during the race!",
            "Done! {}'s tank is dry!",
            "Finished! {} can't fly anymore."
        ]
        # minimal amount of fuel required for 1 sec flight
        min_move_fuel = self.velocity * self.perf
        if self.fuel_cap == 0:  # empty tank -> no move
            print((random.choice(text_FuelMessage_empty)).format(self.name))
            self.move = 0
            return False
        elif self.fuel_cap < min_move_fuel:  # too little for minimal flight
            print((random.choice(text_FuelMessage_shortage)).format(self.name))
            self.move = 0
            return False
        elif self.fuel_cap < (time * min_move_fuel):  # too little for full flight
            print((random.choice(text_FuelMessage_partial)).format(self.name))
            time = (self.fuel_cap // min_move_fuel)
            fuel_needed = (time * min_move_fuel)
            self.fuel_cap -= fuel_needed
            return fuel_needed
        else:  # full flight
            print((random.choice(text_FuelMessage_full)).format(self.name))
            fuel_needed = (time * min_move_fuel)
            self.fuel_cap -= fuel_needed
            return fuel_needed

    def flight(self, travel_data):
        """
        flight(self -> class object | travel_data -> value float :
        value from check_fuel_state)
        return : bool value

        Method to get info about the flight distance of rocket
        Takes travel data (stored values returned from
        function check_fuel_state) changes height class parameter
        Returns boolean value
        """

        if travel_data == False:  # no or too little fuel stops the race
            return False
        elif (travel_data) != 0:  # enough fuel given by other function
            distance = travel_data / self.perf
            self.height += round(distance)
            return True
        else:
            print("wtf?")  # something goes wrong


def drawn_race_time():
    """
    drawn_race_time( None )
    return : int

    The picked time for race, 5 seconds
    increased/decreased up to 4 seconds
    """
    time = 5
    time += (random.randint(-4, 4))
    return time


def check_choice(choice):
    """
    check_choice( choice -> variable : str )
    return : bool False value or None

    Function to determine if chosen player belongs to a list
    and remove it from it only if the ship is unable to move (move == 0).
    Return boolean value or nothing.
    """
    text_RaceStop = [
        "The race stops for {}",
        "And this is the end of race for {}",
        "So {} finished in this competition",
        "The {} stopped. It won't fly no more",
        "And this is the end for {}"
    ]
    if choice in contestant_list:
        if choice in list_rocket1:
            if rocket1.move == 0:
                contestant_list.remove(rocket1.name)
                contestant_list.remove(rocket1.pilot)
                print((random.choice(text_RaceStop)).format(choice))
                return False
            else:
                pass
        if choice in list_rocket2:
            if rocket2.move == 0:
                contestant_list.remove(rocket2.name)
                contestant_list.remove(rocket2.pilot)
                print((random.choice(text_RaceStop)).format(choice))
                return False
            else:
                pass
        if choice in list_rocket3:
            if rocket3.move == 0:
                contestant_list.remove(rocket3.name)
                contestant_list.remove(rocket3.pilot)
                print((random.choice(text_RaceStop)).format(choice))
                return False
            else:
                pass
        if choice in list_rocket4:
            if rocket4.move == 0:
                contestant_list.remove(rocket4.name)
                contestant_list.remove(rocket4.pilot)
                print((random.choice(text_RaceStop)).format(choice))
                return False
            else:
                pass
        if choice in list_rocket5:
            if rocket5.move == 0:
                contestant_list.remove(rocket5.name)
                contestant_list.remove(rocket5.pilot)
                (random.choice(text_RaceStop)).format(choice)
                return False
            else:
                pass
    else:
        return False


# data and statistics for each rocket given to class init
list_rocket1 = [
    "Star Chaser",
    195000,
    5.8,
    750,
    0.8,
    "Tim White"
]
list_rocket2 = [
    "Flash Runner",
    735000,
    25,
    640,
    0.75,
    "Todd H. Watson"
]
list_rocket3 = [
    "Crimson Light",
    250150,
    7,
    810,
    0.9,
    "Garry Goodspeed"
]
list_rocket4 = [
    "Invader Red",
    5950000,
    180,
    700,
    0.68,
    "David Dewinter"
]
list_rocket5 = [
    "Dread Renegade",
    11500000,
    350,
    1020,
    0.87,
    "Clarence Polkawitz"
]
moves = 10

rocket1 = Rocket(list_rocket1)
rocket2 = Rocket(list_rocket2)
rocket3 = Rocket(list_rocket3)
rocket4 = Rocket(list_rocket4)
rocket5 = Rocket(list_rocket5)


race_choices_pilot = enum.Enum(
    "Pilot-name",
    (list_rocket1[5],
     list_rocket2[5],
     list_rocket3[5],
     list_rocket4[5],
     list_rocket5[5])
)
race_choices_rocket = enum.Enum(
    "Rocket-name",
    (list_rocket1[0],
     list_rocket2[0],
     list_rocket3[0],
     list_rocket4[0],
     list_rocket5[0])
)

contestant_list = race_choices_pilot._member_names_ + \
    race_choices_rocket._member_names_

winner_list = (rocket1, rocket2, rocket3, rocket4, rocket5)


# main program executable code - the race process
print("Welcome to Toro Regata!!\n")
print("We have 5 contestants each piloting his own ship!")
print("I'm presenting {pilot}, piloting the {ship}!!".format(
    pilot=rocket1.pilot, ship=rocket1.name))
print("Next one is {pilot}, controlling amazing {ship}!!".format(
    pilot=rocket2.pilot, ship=rocket2.name))
print("Another contestant {pilot}, in his mighty \
        and legendary {ship}!!!".format(pilot=rocket3.pilot, ship=rocket3.name)
      .replace(" " * 8, ""))
print("And the next rider is {pilot} flying oustanding {ship}!!!".format(
    pilot=rocket4.pilot, ship=rocket4.name))
print("The last pilot, {pilot}, steering the magnificent {ship}!!".format(
    pilot=rocket5.pilot, ship=rocket5.name))
print("Let's begin the race!! Choose each and every pilot to move his ship\n\
      10 times at most and see who will be first!".replace(" " * 6, ""))

while True:
    if sum((element.move
            for element in winner_list
            )) == 0:
        print("That's is the end of Toro Regata!")
        winners = sorted(winner_list,
                         reverse=True, key=lambda rocket: rocket.height)
        print("The winner is {ship} with the amazing distance,\
            {distance} meters".format(ship=winners[0].name,
                                      distance=winners[0].height)
              .replace(" " * 12, " "))
        print(
            "The second arrived {ship1} traveled for \
                {distance1} meters.\
             Third finished {ship2}, flew only {distance2}.".format(
                ship1=winners[1].name,
                distance1=winners[1].height,
                ship2=winners[2].name,
                distance2=winners[2].height).replace(" " * 16, "")
            .replace(" " * 12, ""))
        print("Congratulations! {} won the main prize! The Mooncake!".format(
            winners[0].pilot))
        print("The rest... {ship3} passed {distance3} meters and\
                {ship4} barely {distance4}...".format(
            ship3=winners[3].name,
            ship4=winners[4].name,
            distance3=winners[3].height,
            distance4=winners[4].height
        ).replace(" " * 16, " ")
        )
        break
    else:
        version = input(
            "Decide if You want to move players on Your own\n"
            "or let program to move randomly:\n"
            "manual (man) or automatic (auto)\n"
        ).strip().lower()
        if version in ("manual", "man"):
            print("Move each one as You please!\nStart the race!")
            while True:
                choice = input(
                    "Type the name of a pilot or the ship\n").strip().title()
                print("You chose {}".format(choice))
                try:  # make this loop to check after all moves are done
                    choice_move = int(
                        input("How many times You want to move:\n"))
                    if choice in list_rocket1 and contestant_list:
                        for pass_ in range(choice_move):
                            check = check_choice(choice)
                            if check != False:
                                chance = rocket1.startup()
                                if chance == True:
                                    time = drawn_race_time()
                                    the_fuel = rocket1.check_fuel_state(time)
                                    if the_fuel == False:
                                        break
                                    else:
                                        rocket1.flight(the_fuel)
                                else:
                                    continue
                            else:
                                break
                    elif choice in list_rocket2:
                        for pass_ in range(choice_move):
                            check = check_choice(choice)
                            if check != False:
                                chance = rocket2.startup()
                                if chance == True:
                                    time = drawn_race_time()
                                    the_fuel = rocket2.check_fuel_state(time)
                                    if the_fuel == False:
                                        break
                                    else:
                                        rocket2.flight(the_fuel)
                                else:
                                    continue
                            else:
                                break
                    elif choice in list_rocket3:
                        for pass_ in range(choice_move):
                            check = check_choice(choice)
                            if check != False:
                                chance = rocket3.startup()
                                if chance == True:
                                    time = drawn_race_time()
                                    the_fuel = rocket3.check_fuel_state(time)
                                    if the_fuel == False:
                                        break
                                    else:
                                        rocket3.flight(the_fuel)
                                else:
                                    continue
                            else:
                                break
                    elif choice in list_rocket4:
                        for pass_ in range(choice_move):
                            check = check_choice(choice)
                            if check != False:
                                chance = rocket4.startup()
                                if chance == True:
                                    time = drawn_race_time()
                                    the_fuel = rocket4.check_fuel_state(time)
                                    if the_fuel == False:
                                        break
                                    else:
                                        rocket4.flight(the_fuel)
                                else:
                                    continue
                            else:
                                break
                    elif choice in list_rocket5:
                        for pass_ in range(choice_move):
                            check = check_choice(choice)
                            if check != False:
                                chance = rocket5.startup()
                                if chance == True:
                                    time = drawn_race_time()
                                    the_fuel = rocket5.check_fuel_state(time)
                                    if the_fuel == False:
                                        break
                                    else:
                                        rocket5.flight(the_fuel)
                                else:
                                    continue
                            else:
                                break
                    else:
                        print("This contestant is out of the game!")
                        print("Please choose another one.")
                        continue
                except ValueError:
                    print("Please provide only integer values.")
                    continue
        elif version == "automatic" or version == "auto":
            print("Let the game begins!")
            for contestant in winner_list:
                for moves in range(contestant.move):
                    time = drawn_race_time()
                    chance = contestant.startup()
                    if chance == True:
                        the_fuel = contestant.check_fuel_state(time)
                        if the_fuel == False:
                            break
                        else:
                            contestant.flight(the_fuel)
                    else:
                        continue
        else:
            print(
                "Input 'auto', 'automatic' for self-acting\n"
                "or 'manual', 'man' for performing race by hand\n"
            )
            continue
